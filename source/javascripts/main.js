"use strict";

let hamburger = document.querySelector(".hamburger");
let mainmenu = document.querySelector(".main-menu");

hamburger.addEventListener("click", (e) => {
  hamburger.classList.toggle("is-active");
  mainmenu.classList.toggle("is-open");
});

window.onload = function() {

  setTimeout(() => {
    let body = document.querySelector("body");
    body.classList.add("anim-in");
  }, 1000)

};